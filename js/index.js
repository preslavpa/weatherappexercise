/**
 * 
 */

$(function () {
	var panel = new CityPanel();
	new AddCityDialog(panel);
	
	
})

function AddCityDialog(panel) {
	
	this.panel = panel;
	
	this.$btnNew = $('#btn-add-new');
	this.$dialog = $('#dialog-form');
	this.$city = $('#city');
	this.$dateFrom = $('#date-from');
	this.$dateTo = $('#date-to');
	this.$numberOfResults = $('#number-of-results');
	this.$slider = $('#number-of-results-slider');
	this.$mode = $('#mode');
	this.initElements();
	
}

AddCityDialog.OPEN_WEATHER_URL = 'http://api.openweathermap.org/data/2.5/history/city';

AddCityDialog.MODE_CNT = 'mode_cnt';
AddCityDialog.MODE_END_DATE = 'mode_end_date';

AddCityDialog.prototype.initElements = function() {
	
	var _this = this;
	
	this.$btnNew.button().on('click', function() {
		_this.$dialog.dialog('open');
	});
	
	this.$dateFrom.datepicker();
	this.$dateTo.datepicker();
	
	this.$dialog.dialog({
		autoOpen: false,
		height: 390,
		width: 500,
		modal: true,
		buttons: {
			'Add' : function() {
				_this.addCity();
			},
			'Close': function() {
				_this.$dialog.dialog('close');
			}
		},
		open: function() {
		}
	});
	
	this.$slider.slider({
		min: 1,
		max: 24,
		slide: function(event, ui) {
			_this.$numberOfResults.val(ui.value);
		}
	});
	
	this.$city.autocomplete({
		source: cities
	});
	
	this.$mode.on('change', function() {
		$('#date-to-container, #slider-container').toggle()
	})
}

AddCityDialog.prototype.addCity = function() {
	var data = {
		'city' : this.$city.val(),
		'start': this.parseDate(this.$dateFrom.val()),
		'end': this.parseDate(this.$dateTo.val()),
		'cnt': this.$numberOfResults.val(),
		'mode': this.$mode.val()
	};
	
	var errors = this.validate(data);
	if (errors.length) {
		alert('Not able to add new city\n' + errors.join('\n'));
		
		return;
	}
	
	this.callOpenWeather(data);
}

AddCityDialog.prototype.parseDate = function(dateStr) {
	var dateArray = dateStr.split('/');
	
	var date = new Date();
	date.setFullYear(dateArray[2], parseInt(dateArray[0]) - 1, dateArray[1]);
	
	return Math.floor(date.getTime() / 1000);
};

AddCityDialog.prototype.validate = function(data) {
	var messages = [];
	
	if (!data.city || $.inArray(data.city, cities) === -1) {
		messages.push('Invalid City value');
	}
	
	if (!data.start || data.start > data.end) {
		messages.push('Invalid start time');
	}
	
	if (data.mode == AddCityDialog.MODE_END_DATE && (!data.end || data.start > data.end)) {
		messages.push('Invalid end time');
	}
	
	if (data.cnt < 1 || data.cnt > 24) {
		messages.push('Invalid number of results');
	}
	
	return messages;
};

AddCityDialog.prototype.callOpenWeather  = function (data) {
	var _this = this;
	$.ajax({
		url: this.buildUrl(data),
		dataType: 'jsonp',
		jsonp:'callback'
	}).done(function(response) {
		_this.onResponse(response, data);
	});
};

AddCityDialog.prototype.buildUrl = function(data) {
	var params = [];
	
	params.push('q=' + encodeURIComponent(data.city) + ',BG');
	params.push('start=' + data.start);
	
	if (this.$mode.val() == AddCityDialog.MODE_END_DATE) {
		params.push('end=' + data.end);
	} else {
		params.push('cnt=' + data.cnt);		
	}
	
	return AddCityDialog.OPEN_WEATHER_URL + '?' + params.join('&');
};

AddCityDialog.prototype.onResponse = function(response, request) {
	this.panel.addPanel(request.city,response);
};

function CityPanel() {
	this.$container = $('#accordion');
	this.$container.accordion();
}

CityPanel.ICON_URL = 'http://openweathermap.org/img/w/';

CityPanel.prototype.addPanel = function(name, data) {
	this.$container.append('<h3>' + name + '</h3>');
	this.$container.append('<div>' + this.getWeatherHTML(data) + '</div>');
	this.$container.accordion('refresh');
}

CityPanel.prototype.getWeatherHTML = function(data) {
	// 37C Cloudy
	var html = '';
	for (var i = 0; i < data.list.length; i++) {	
		//console.log(data.list[i], data.list[i].main.temp);
		html += '<p><span class="degrees">' + (parseFloat(data.list[i].main.temp) - 273.15).toFixed(2) +'&nbsp;&deg;</span><span class="weather-icon">\
		<img src="' +  CityPanel.ICON_URL + '/' + data.list[i].weather[0].icon +'.png"></span>'+
		'<span class="description">' + data.list[i].weather[0].description + '</span>'
		+'</p>';
	}
	
	return html;
}